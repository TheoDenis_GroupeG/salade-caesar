import string

def check_key(key):
    """
        checks increments count
    """
    if key>20 or key<1:
        raise Exception("range is 1 to 20.")

def check_sentence(sentence):
    """
        checks text's emptyness
    """
    if sentence is None:
        raise Exception("Text must not be empty.")

def crypt_no_circle(sentence, key):
    """
        sentence: user's input
        key: cryptage key

        This function will add the key value to the ascii numeric value of each character. 

        Example:
        >>> crypt("abc", 1)
        'bcd'
        >>> crypt("abc", 3)
        'def'
    """
    check_key(key)
    check_sentence(sentence)

    result = ''
    for character in sentence:
        result = result + chr(ord(character) + key)

    return result

def decrypt_no_circle(sentence, key):
    """
        This function returns the original text, coded with this key.

        Example:
        >>> code("bcd", 1)
        'abc'
        >>> code ("def", 3)
        'abc'
    """
    result = ''
    result=result+crypt_no_circle(sentence, -key)

    return result

def crypt_circle(sentence, key):
    """
        sentence: user's input
        key: cryptage key

        This function will add the key value to the ascii numeric value of each character, in circular mode.

        :Example:
        >>> code("abc", 1)
        'bcd'
        >>> code ("xyz", 1)
        'yza"
    """
    check_key(key)
    check_sentence(sentence)

    result = ''
    for character in sentence:
        if ord(character)>90 or ord(character)>121:
            result = result + chr(ord(character) - 26 + key)
        else : result = result + chr(ord(character))
    
    return result

def decrypt_circle(sentence, key):
    """
        sentence: user's input
        key: cryptage key

        This function returns the original text, coded with this key in circular mode.

        :Example:
        >>> code("bcd", 1)
        'abc'
        >>> code ("yza", 1)
        'xyz'
    """
    check_key(key)
    check_sentence(sentence)
    
    result = ''
    for character in sentence:
        if ord(character) > 65 or ord(character) > 97:
            result = result + chr(ord(character) + 26 - key)
        else : result = result + chr(ord(character) - key)
    
    return result