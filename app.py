from tkinter import * 
from tkinter import ttk
from tkinter.messagebox import *
from crypt import *

def callback_crypt_no_circle():

    clear.set(crypt_no_circle(text_input.get(), int(key.get()))) 

def callback_decrypt_no_circle():
    crypted.set(decrypt_no_circle(text_input.get(), int(key.get())))

def callback_crypt_circle():

    clear.set(crypt_circle(text_input.get(),int(key.get()))) 

def callback_decrypt_circle():
    crypted.set(decrypt_circle(text_input.get(),int(key.get())))


root = Tk()

root.title("CAESAR CRYPTAGE")
root.minsize(800, 150)
root.columnconfigure(0, weight=2)
root.columnconfigure(2, weight=3)
root.columnconfigure(1, weight=2)
root.columnconfigure(1, weight=2)
root.columnconfigure(4, weight=2)
root.rowconfigure(3, weight=1)


label = Label(root, text="TESTER C'EST DOUTER.")
label.grid(row=1, column=1)

clear = "Please Insert a Value."
clear = StringVar(root)
crypted = StringVar(root)

text_input = Entry(root, textvariable=crypted)
text_input.grid(row=2, column=0, padx=10, pady=10, sticky='nsew')

text_output = Entry(root, textvariable=clear)
text_output.grid(row=2, column=2, padx=10, pady=10, sticky='nsew')

key = ttk.Spinbox(root, from_=1, to=20)
key.grid(row=3, column=1)

crypt_no_circle_button = Button(root, text='Crypt', command=callback_crypt_no_circle)
crypt_no_circle_button.grid(row=4, column=0)

decrypt_no_circle_button = Button(root,text='Decrypt', command=callback_decrypt_no_circle)
decrypt_no_circle_button.grid(row=4, column=1)

crypt_circle_button = Button(root,text='Crypt - Circle', command=callback_crypt_circle)
crypt_circle_button.grid(row=4, column=2)

decrypt_circle_button = Button(root,text='Decrypt - Circle', command=callback_decrypt_circle)
decrypt_circle_button.grid(row=4, column=3)

root.mainloop()