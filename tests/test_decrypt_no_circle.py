import unittest
from crypt import *

def test_decrypt_no_circle(self):
        self.assertEqual(decrypt_no_circle("bcd", 1), "abc")
        self.assertEqual(decrypt_no_circle("abz", 1), "`ay")