import unittest
from crypt import *

def test_decrypt_circle(self):
        self.assertEqual(decrypt_circle("bcd", 1), "abc")
        self.assertEqual(decrypt_circle("acd", 1), "zbc")