import unittest
from crypt import *

class FonctionTestCase(unittest.TestCase):

    def test_crypt_no_circle(self):
        self.assertEqual(crypt_no_circle("abc", 1), "bcd")
        self.assertEqual(crypt_no_circle("abz", 1), "bc{")
        self.assertEqual(crypt_no_circle('a', 1), 'b')
        self.assertEqual(crypt_no_circle('Test', 8), '\m{|')
        self.assertEqual(crypt_no_circle('ZEvent', 0), 'ZEvent')

        self.assertNotEqual(crypt_no_circle('Tester c est douter.', -2), '')
        self.assertNotEqual(crypt_no_circle('Tester c est douter.', 1), 'Tester c est douter.')